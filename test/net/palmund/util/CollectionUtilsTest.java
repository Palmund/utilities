package net.palmund.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

public class CollectionUtilsTest {
	private Collection<String> strings;
	
	@SuppressWarnings("serial")
	@Before
	public void setUp() throws Exception {
		strings = new ArrayList<String>() {{
			add("hej");
			add("med");
			add("dig");
		}};
	}

	@Test
	public void testGetElementAtIndex() {
		String stringAtIndex = CollectionUtils.getElementAtIndex(strings, 1);
		assertEquals("med", stringAtIndex);
	}
}