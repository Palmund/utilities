package net.palmund.util;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageUtils {
	public static BufferedImage rotate(BufferedImage image, int degrees) {
		BufferedImage rotatedImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		Graphics2D graphics2D = rotatedImage.createGraphics();
		graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		graphics2D.rotate(Math.toRadians(degrees), image.getWidth() / 2, image.getHeight() / 2);
		graphics2D.drawImage(image, null, 0, 0);
		return rotatedImage;
	}

	public static boolean saveImage(BufferedImage image, File file) throws IOException {
		String imageExtension = getImageExtension(file);
		if (imageExtension == null || imageExtension.equals("")) {
			throw new IOException("Unknown or empty image extension");
		}
		return ImageIO.write(image, imageExtension, file);
	}
	
	private static String getImageExtension(File file) {
		String filename = file.getName();
		int index = filename.lastIndexOf('.');
		if (index > 0 && index < filename.length() - 1) {
			return filename.substring(index+1).toLowerCase();
		}
		return null;
	}

	public static Image scale(Image image, Dimension dimension, int hints) {
		return scale(image, dimension.width, dimension.height, hints);
	}
	
	public static Image scale(Image image, int width, int height, int hints) {
		System.out.printf("Scaling image to %dx%d\n", width, height);
		return image.getScaledInstance(width, height, hints);
	}
}