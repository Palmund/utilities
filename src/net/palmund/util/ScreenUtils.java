package net.palmund.util;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

public class ScreenUtils {
	/**
	 * Convenience method for calling {@link
	 * Toolkit.getDefaultToolkit().getScreenDimension()}
	 * 
	 * @return the screens {@link Dimension}
	 */
	public static Dimension getScreenDimension() {
		return Toolkit.getDefaultToolkit()
						.getScreenSize();
	}

	public static BufferedImage createScreenCapture(Rectangle captureSize) throws AWTException {
		return new Robot().createScreenCapture(captureSize);
	}
}