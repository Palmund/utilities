package net.palmund.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

public class PropertiesUtils {
	public static Properties loadProperties(String propertyFile) {
		return loadProperties(new Properties(), propertyFile);
	}
	
	public static Properties loadProperties(Properties properties, String propertyFile) {
		InputStream in = PropertiesUtils.class.getResourceAsStream(propertyFile);
		try {
			properties.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;
	}
	
	public static void merge(Properties master, Properties merger) {
		Set<Entry<Object, Object>> entrySet = merger.entrySet();
		for (Entry<Object, Object> entry : entrySet) {
			Object key = entry.getKey();
			if (!master.containsKey(key)) {
				master.put(key, entry.getValue());
			}
		}
	}
}