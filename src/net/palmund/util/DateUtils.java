package net.palmund.util;

import java.util.Date;

public class DateUtils {
	private static final int DAY = 0;
	private static final int MONTH = 1;
	private static final int YEAR = 2;
	
	@SuppressWarnings("deprecation")
	public static Date makeDate(String date) {
		String[] chunks = date.split("-");
		int day = Integer.parseInt(chunks[DAY]);
		int month = Integer.parseInt(chunks[MONTH]);
		int year = Integer.parseInt(chunks[YEAR]);
		return new Date(year, month, day);
	}
}