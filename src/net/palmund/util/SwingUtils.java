package net.palmund.util;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class SwingUtils {
public static final String LAF_NIMBUS = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
	
	public static void centerComponentOnScreen(Container container) {
		Dimension screenDimension = ScreenUtils.getScreenDimension();
		Rectangle containerRectangle = new Rectangle();
		containerRectangle.width = container.getWidth();
		containerRectangle.height = container.getHeight();
		containerRectangle.x = (screenDimension.width / 2) - (containerRectangle.width / 2);
		containerRectangle.y = (screenDimension.height / 2) - (containerRectangle.height / 2);
		container.setBounds(containerRectangle);
	}

	public static void setAlwaysOnTop(Dialog dialog, boolean b) {
		if (b) {
			dialog.setModalityType(ModalityType.APPLICATION_MODAL);
		} else {
			dialog.setModalityType(ModalityType.MODELESS);
		}
	}
	
	public static void setEscapable(RootPaneContainer container) {
		setEscapable(container.getRootPane());
	}

	public static void setEscapable(JRootPane rootPane) {
		EscapeAction escapeAction = new EscapeAction();
		HotkeyUtils.registerHotkeyOnComponent(rootPane, KeyEvent.VK_ESCAPE, escapeAction);
	}

	private static class EscapeAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		private static final String KEY_STROKE_AND_KEY = "ESCAPE";
		private static final KeyStroke ESCAPE_KEY_STROKE = KeyStroke.getKeyStroke(KEY_STROKE_AND_KEY);

		public EscapeAction() {
			super("Escape");
		}

		/**
		 * Implement the Escape Action. First attempt to hide a popup menu. If
		 * no popups are found then dispose the window.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			/*
			 * When a popup is visible the root pane of the Window will
			 * (generally) have focus.
			 */

			Component component = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
			JComponent rootPane = (JComponent) component;

			/*
			 * In some cases a component added to a popup menu may have focus,
			 * but we need the root pane to check for popup menu key bindings.
			 */

			if (!(rootPane instanceof JRootPane)) {
				rootPane = (JComponent) SwingUtilities.getAncestorOfClass(JRootPane.class, component);
			}

			/*
			 * Hide the popup menu when an ESCAPE key binding is found,
			 * otherwise dispose the Window.
			 */
			ActionListener escapeAction = getEscapeAction(rootPane);
			if (escapeAction != null) {
				escapeAction.actionPerformed(null);
			} else {
				Window window = SwingUtilities.windowForComponent(component);
				window.dispose();
			}
		}

		private ActionListener getEscapeAction(JComponent rootPane) {
			/*
			 * Search the parent InputMap to see if a binding for the ESCAPE key
			 * exists. This binding is added when a popup menu is made visible
			 * (and removed when the popup menu is hidden).
			 */

			InputMap inputMap = rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
			if (inputMap == null) {
				return null;
			}

			inputMap = inputMap.getParent();
			if (inputMap == null) {
				return null;
			}

			Object[] keys = inputMap.keys();
			if (keys == null) {
				return null;
			}

			for (Object keyStroke : keys) {
				if (keyStroke.equals(ESCAPE_KEY_STROKE)) {
					Object key = inputMap.get(ESCAPE_KEY_STROKE);
					return rootPane.getActionMap().get(key);
				}
			}
			return null;
		}

		// /**
		// * Register the EscapeAction on the specified JRootPane
		// *
		// * @param rootPane
		// * the JRootPane the EscapeAction is registered with
		// */
		// public void register(JRootPane rootPane) {
		// rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
		// .put(ESCAPE_KEY_STROKE, KEY_STROKE_AND_KEY);
		// rootPane.getActionMap().put(KEY_STROKE_AND_KEY, this);
		// }
	}
	
	/**
	 * Attempts to set the LookAndFeel of the ui.
	 * 
	 * @param path path to the <code>LookAndFeel</code>
	 */
	public static void setLookAndFeel(String path) {
		try {
//			UIManager.put("control", new Color(240, 240, 240));
//			UIManager.put("nimbusBase", new Color(125, 156, 159));
//			UIManager.put("nimbusBlueGrey", new Color(165, 174, 182));
			UIManager.setLookAndFeel(path);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}

	public static Font calculateFontSize(String string, Font font, Dimension dimension, Graphics g) {
		int size = getMaxFittingFontSize(g, font, string, dimension.width, dimension.height);
		return createResizedFont(font, size);
	}
	
	private static int getMaxFittingFontSize(Graphics g, Font font, String string, int width, int height) {
		int minSize = 0;
		int maxSize = 288;
		int curSize = font.getSize();
		
		while (maxSize - minSize > 2) {
			FontMetrics fm = g.getFontMetrics(new Font(font.getName(), font.getStyle(), curSize));
			int fontWidth = fm.stringWidth(string);
			int fontHeight = fm.getLeading() + fm.getMaxAscent()
					+ fm.getMaxDescent();

			if ((fontWidth > width) || (fontHeight > height)) {
				maxSize = curSize;
				curSize = (maxSize + minSize) / 2;
			} else {
				minSize = curSize;
				curSize = (minSize + maxSize) / 2;
			}
		}
		return curSize;
	}

	private static Font createResizedFont(Font font, int size) {
		return new Font(font.getName(), font.getStyle(), size);
	}
}