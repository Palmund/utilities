package net.palmund.util;

public class StringUtils {
	public static String reverse(String string) {
		char[] characters = string.toCharArray();
		char[] reversed = new char[characters.length];
		int n = 0;
		for (int i = characters.length-1; i >= 0; i--) {
			reversed[n++] = characters[i];
		}
		return new String(reversed);
	}
	
	public static boolean isNullOrEmpty(String string) {
		return string == null || string.equals("");
	}
}