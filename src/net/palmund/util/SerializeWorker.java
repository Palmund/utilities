package net.palmund.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URL;

public class SerializeWorker<T> {
private File file;
	
	public SerializeWorker(File file) {
		this.file = file;
	}
	
	public boolean serialize(T object) {
		boolean objectSerialized = false;
		try {
			FileOutputStream fileOut = new FileOutputStream(file);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			System.out.println(object instanceof Serializable);
			out.writeObject(object);
			objectSerialized = true;
			out.close();
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return objectSerialized;
	}
	
	@SuppressWarnings("unchecked")
	public T deserialize() throws ClassNotFoundException {
		T deserializedObject = null;
		try {
			FileInputStream fileIn = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			deserializedObject = (T) in.readObject();
			fileIn.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return deserializedObject;
	}
	
	@SuppressWarnings("unchecked")
	public T deserializeAs(Class<T> klass) throws ClassNotFoundException {
		T deserializedObject = null;
		try {
			FileInputStream fileIn = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			deserializedObject = (T) in.readObject();
			fileIn.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return deserializedObject;
	}
	
	@SuppressWarnings("unchecked")
	public T deserializeResourceAs(URL url, Class<T> klass) throws ClassNotFoundException {
		T deserializedObject = null;
		try {
			ObjectInputStream in = new ObjectInputStream(url.openStream());
			deserializedObject = (T) in.readObject();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return deserializedObject;
	}
}