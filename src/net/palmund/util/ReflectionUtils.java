package net.palmund.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class ReflectionUtils {
	public static <T, A extends Annotation> Field[] getAnnotatedFields(T object, Class<A> annotationClass) {
		return getAnnotatedFields(object.getClass(), annotationClass);
	}
	
	public static <T, A extends Annotation> Field[] getAnnotatedFields(Class<T> klass, Class<A> annotationClass) {
		Field[] fields = klass.getDeclaredFields();
		ArrayList<Field> annotatedFields = new ArrayList<Field>();
		for (Field field : fields) {
			A annotation = field.getAnnotation(annotationClass);
			if (annotation != null) {
				annotatedFields.add(field);
			}
		}
		return annotatedFields.toArray(new Field[0]);
	}
	
	public static <T, A extends Annotation> Field getSingleFieldWithAnnotation(Class<T> klass, Class<A> annotationClass) {
		Field[] fields = klass.getDeclaredFields();
		for (Field field : fields) {
			A annotation = field.getAnnotation(annotationClass);
			if (annotation != null) {
				return field;
			}
		}
		return null;
	}
	
	public static <A extends Annotation> boolean containsFieldWithAnnotation(Class<?> klass, Class<A> annotationClass) {
		Field[] fields = klass.getDeclaredFields();
		for (Field field : fields) {
			if (field.getAnnotation(annotationClass) != null) {
				return true;
			}
		}
		return false;
	}
	
	public static <T, A extends Annotation> boolean isAnnotatedWith(T object, Class<A> annotationClass) {
		return isAnnotatedWith(object.getClass(), annotationClass);
	}
	
	public static <A extends Annotation> boolean isAnnotatedWith(Class<?> klass, Class<A> annotationClass) {
		return klass.getAnnotation(annotationClass) != null;
	}
	
	public static <A extends Annotation> boolean isAnnotatedWith(Field field, Class<A> annotationClass) {
		return field.getAnnotation(annotationClass) != null;
	}

	public static Method getSetterMethod(Field field, Class<?> entityClass) throws NoSuchMethodException {
		String methodName = generateSetterMethodName(field);
		Method setterMethod = entityClass.getDeclaredMethod(methodName, field.getType());
		return setterMethod;
	}
	
	private static String generateSetterMethodName(Field field) {
		String fullname = field.getName();
		char firstChar = fullname.toCharArray()[0];
		char capitalizedFirstChar = Character.toUpperCase(firstChar);
		return "set"+capitalizedFirstChar + fullname.substring(1);
	}

	public static Method getGetterMethod(Field field, Class<?> entityClass) throws NoSuchMethodException {
		String methodName = generateGetterMethodName(field);
		Method setterMethod = entityClass.getDeclaredMethod(methodName);
		return setterMethod;
	}
	
	private static String generateGetterMethodName(Field field) {
		String fullname = field.getName();
		char firstChar = fullname.toCharArray()[0];
		char capitalizedFirstChar = Character.toUpperCase(firstChar);
		return "get"+capitalizedFirstChar + fullname.substring(1);
	}
}