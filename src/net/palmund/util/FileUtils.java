package net.palmund.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileUtils {
	private static final String TEMP_EXTENSION = ".temp";

	public static boolean deleteFolder(File folder) throws IOException {
		if (folder.isDirectory()) {
			File[] files = folder.listFiles();
			for (File element : files) {
				if (element.isDirectory()) {
					deleteFolder(element);
				} else {
					deleteFile(element);
				}
			}
		}
		boolean result = folder.delete();
		if (!result) {
			throw new FileNotFoundException("Failed to delete file: " + folder);
		}
		return result;
	}

	public static boolean deleteFile(File file) throws IOException {
		if (file.isDirectory()) {
			throw new IllegalArgumentException("Argument cannot be a directory!");
		}
		boolean result = file.delete();
		if (!result) {
			throw new FileNotFoundException("Failed to delete file: " + file);
		}
		return result;
	}

	/**
	 * Generates a temporary file that resides in <code>baseDir</code>.
	 * 
	 * @param baseDir
	 *            the temporary file's directory
	 * @return a {@link File} object representing the temporary file
	 * @throws IOException
	 *             if the file could not be created
	 */
	public static synchronized File generateTempFile(String baseDir) throws IOException {
		File tempFile = new File(System.currentTimeMillis() + TEMP_EXTENSION);
		tempFile.createNewFile();
		return tempFile;
	}

	/**
	 * Merges {@code file2} with {@code file1} by appending the content from
	 * {@code file2} to the bottom of the {@code file1}.
	 * 
	 * @param file1
	 *            the {@link File} to merge with
	 * @param file2
	 *            the {@link File} to be merged
	 * 
	 * @throws IOException
	 */
	public static synchronized void mergeFiles(File file1, File file2) throws IOException {
		Scanner in = new Scanner(file2);
		BufferedWriter out = new BufferedWriter(new FileWriter(file1, true));
		while (in.hasNextLine()) {
			String line = in.nextLine();
			out.write(line);
			out.newLine();
		}
		out.flush();
		in.close();
		out.close();
	}
}