package net.palmund.util;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;

public class DialogUtils {
	public static void setEscapable(JDialog dialog) {
		SwingUtils.setEscapable(dialog);
	}

	public static void centerDialogOnComponent(Dialog dialog,
			Component component) {
		Window window = SwingUtilities.windowForComponent(component);
		centerDialogOnWindow(dialog, window);
	}

	public static void centerDialogOnWindow(Dialog dialog, Window window) {
		Point point = calculatePoint(dialog, window);
		dialog.setLocation(point);
	}

	private static Point calculatePoint(Dialog dialog, Window window) {
		Point windowLocation = window.getLocation();
		Dimension windowSize = window.getSize();
		Dimension dialogSize = dialog.getSize();
		int x = windowLocation.x + (windowSize.width - dialogSize.width) / 2;
		int y = windowLocation.y + (windowSize.height - dialogSize.height) / 2;
		return new Point(x, y);
	}

	public static void setAlwaysOnTop(Dialog dialog) {
		dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
	}

	/**
	 * Sets the <code>dialog</code> to be always on top. Whenever the dialog is
	 * disposed <code>parent</code> will be notified through
	 * {@link JComponent#requestFocus()}
	 * 
	 * @param dialog
	 * @param parent
	 *            the component to be notified when the dialog is disposed
	 */
	public static void setAlwaysOnTop(Dialog dialog, final JComponent parent) {
		setAlwaysOnTop(dialog);
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				parent.requestFocus();
				super.windowClosed(e);
			}
		});
	}
}