package net.palmund.util;

import java.awt.event.MouseEvent;

public class MouseUtils {
	public static boolean isDoubleClick(MouseEvent e) {
		return isPrimaryDoubleClick(e) || isAlternateDoubleClick(e);
	}
	
	public static boolean isPrimaryDoubleClick(MouseEvent e) {
		return (e.getButton() == 1) && (e.getClickCount() == 2);
	}

	public static boolean isAlternateDoubleClick(MouseEvent e) {
		return (e.getButton() == 2) && (e.getClickCount() == 2);
	}
}