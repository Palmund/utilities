package net.palmund.util;

import java.util.Collection;

public class CollectionUtils {
	public static <T> T getElementAtIndex(Collection<T> collection, int index) {
		if (index >= collection.size()) {
			throw new ArrayIndexOutOfBoundsException(index+" >= "+collection.size());
		}
		T foundElement = null;
		int i = 0;
		for (T element : collection) {
			if (i == index) {
				foundElement = element;
				break;
			}
			i++;
		}
		return foundElement; // This return statement will never be reached!
	}
}