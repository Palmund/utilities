package net.palmund.util;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

public class HotkeyUtils {
	public static void registerHotkeyOnComponent(JComponent component, int keyStroke, Action action) {
		KeyStroke key = getKeyStroke(keyStroke);
		registerHotkeyOnComponent(component, key, action);
	}
	
	public static void registerHotkeyOnComponent(JComponent component, String keyStroke, Action action) {
		KeyStroke key = getKeyStroke(keyStroke);
		registerHotkeyOnComponent(component, key, action);
	}
	
	public static void registerHotkeyOnComponent(JComponent component, KeyStroke keyStroke, Action action) {
		InputMap inputMap = component.getInputMap();
		inputMap.put(keyStroke, action.toString());
		ActionMap actionMap = component.getActionMap();
		actionMap.put(action.toString(), action);
	}

	private static KeyStroke getKeyStroke(String keyStroke) {
		return KeyStroke.getKeyStroke(keyStroke);
	}

	private static KeyStroke getKeyStroke(int keyStroke) {
		return KeyStroke.getKeyStroke(keyStroke, 0);
	}
}